#!/bin/bash
files=`git status -s | cut -c4-`
for file in $files; do
  chown -R 1000:1000 $file
  if [ -d "$file" ]; then
    chmod -R 700 $file
  else
    chmod -R 644 $file
  fi
done
