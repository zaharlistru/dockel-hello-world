#!/bin/bash
# Interpreter identifier

# set -e
# Exit on fail

bundle check || bundle install --binstubs="$BUNDLE_BIN"
# Ensure all gems installed. Add binstubs to bin which has been added to PATH in Dockerfile.

rake assets:clean
rake assets:precompile
# rake db:setup
yarn install
rake db:migrate
rm -f /app/shared/pids/unicorn.pid


bundle exec unicorn -c ./config/unicorn.rb



