#!/bin/bash
# Interpreter identifier

# set -e
# Exit on fail

bundle check || bundle install --binstubs="$BUNDLE_BIN"
# Ensure all gems installed. Add binstubs to bin which has been added to PATH in Dockerfile.

yarn install
rake db:migrate
rm -f tmp/pids/server.pid && bundle exec rails s -p 3000 -b '0.0.0.0'
